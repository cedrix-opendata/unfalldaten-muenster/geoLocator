# OpenData-Project
# "Unfalldaten, Münster" 

## Helper: geoLocator 

Location Finder V1.0 of some query-string to Google API
written by Lars Neujeffski (Cedrix) 

Created: 22/07/2018
Software-License: GPLv2

## Result added!

Results of the Google-Map-Queries are now included in the git. Filename: [geolocator.result.csv][geolocator-result]
Consists of **81466 found locations** (out of 97830 total).

[geolocator-result]: https://gitlab.com/cedrix-opendata/unfalldaten-muenster/geoLocator/blob/master/geolocator.result.csv "Download the results."

## Author: Lars Neujeffski 
* [mail](kassierer@cedrix.com)
* [website](https://www.neujeffski.com)
* [twitter](@cedrixAce)
* [twitter](https://twitter.com/cedrixAce)
* [facebook](https://fb.com/neujeffski)

