<?php
/**

	Location Finder V1.0 of some query-string to Google API 
	written by Lars Neujeffski (Cedrix) for OpenData-Project "Unfalldaten, Münster" 

	Created: 22/07/2018
	Software-License: GPLv2
	Author: Lars Neujeffski kassierer@cedrix.com

*/

require('functions.php');


/* Switch "drymode" is turns on real google-api-requests or just cached-versions, saved to disk. */
$drymode = false;


/* 
    Main                            
*/



/* Parse the CSV */

set_time_limit (0);

$i            = 0;
$handle 	  = fopen("ms_unfaelle.csv","r");
//for ($i=0;$i<1300;$i++) {
	$firstrowskip = fgets($handle);
//}
$writehandle  = fopen("result.misc","w+");



if (($handle) && ($writehandle)) {
	while ((($csva=fgetcsv($handle)) !== FALSE ) && ($i++<100000)) {
		//print "$i $csva[1] + $csva[2]\n";
		if (ctype_digit($csva[2])) {
			// Strasse + Hausnummer
			$location = urlencode ("$csva[1] $csva[2], Münster");
		} else {
			$location = urlencode ("$csva[1] Ecke $csva[2], Münster");
		}

		// print $location; 		/* Location String to be found */

		$result = getLocationObject($drymode, $location);
		saveCache($result,$drymode);
		// outputResult($result);
		$obj = examineResult($result);

		// print "\nAND THIS IS THE RESULT:\n\n";
		$tee=returnLatLng($csva[0], $obj, 0);
		print_r($tee);
		fputs($writehandle,"$tee\n");
		print "\n";
	}
} 


?>