<?php
/**

	Location Finder V1.0 Functions

	Written by Lars Neujeffski (Cedrix) for OpenData-Project "Unfalldaten, Münster" 

	Created: 22/07/2018
	Software-License: GPLv2
	Author: Lars Neujeffski kassierer@cedrix.com

*/

/**
 * getLocationObject 
 * 
 * @param boolean	$drymode 	Cached data or Google-Request?
 * @param string    $location   Text-Query of Location
 * @param string    $filename   Filename for cache-file, default location.txt
 * 
 * @return stdClass Object (JSON-format result)
*/

function getLocationObject($drymode,$location, $filename="location.txt") {
	if (!$drymode) {
		$request = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=$location&inputtype=textquery&fields=geometry&key=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
		return file_get_contents($request);        // JSON-formatted Result (stdClass Object)
	} else {
		return file_get_contents($filename);  // JSON-formatted Object (stdClass Object)
	}
}


/**
 * saveCache 
 * 
 * @param string	$result 	Cached data to save
 * @param string    $filename   Filename for cache-file, default location.txt
 * 
 * @return boolean  write-success or true if drymode is on (=dont save in drymode)
*/

function saveCache($result, $filename = "location.txt",$drymode=true) {
	if (!$drymode) {
		return file_put_contents($filename, $result);
	}
	return true;
}


/**
 * outputResult
 * 
 * @param string    $request    Text-query which has been sent to google 
 * @param string    $result 	Result of text-query
 */

function outputResult($result) {
	echo $result;
	echo "\n";
	print_r (json_decode($result));
	echo "\n";
}


/**
 * examineResult
 * 
 * @param string	$result 	json formated response 
 * 
 * @return stdClass Object (JSON-format result)
*/

function examineResult($result) {
	$object = json_decode($result);

	$success = true;

	if (property_exists($object, 'status')) {
		if ($object->status=="OK") {
			// Grab corrdinates and save them
			return $object->candidates[0]->geometry->location;
		} else {
			// Something wrong response by google.
			$success = false;
		}
	}

	return $success;
}

/**
 * returnLatLng 
 * @param 	object 		$obj 		json-converted result-object
 * @param   integer     $id         primary key for SQL-db
 * @param   integer     $format     0=id,lat,lng, 1=SQL-Update-Query, 2=json
 * @return  string  	to work on the database
 *
*/

function returnLatLng ($id, $obj,$format=0) {
	if (is_object($obj)) {

        switch ($format) {

        	case 0:
        		return "$id,$obj->lat,$obj->lng";

        	case 1:
        		return "UPDATE unfalldaten 
        			    SET lat='$obj->lat', lng='$obj->lng' 
        			    WHERE id=$id 
        			    LIMIT 1;";

        	case 2:
        	    $obj->id  =$id;
        		return json_encode($obj);

        }
	}
	return "";
}
?>
